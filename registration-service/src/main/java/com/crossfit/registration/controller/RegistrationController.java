package com.crossfit.registration.controller;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegistrationController {

    // TODO (mzo) to remove as created only for test purposes
    @GetMapping("/registrations")
    public List<String> getRegistrations() {
        return IntStream.range(1, 10)
                .mapToObj(i -> "Registration" + i)
                .collect(Collectors.toList());
    }
}
